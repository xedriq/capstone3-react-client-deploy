import React, { createContext, useState } from 'react'

export const ServiceIconContext = createContext()

export const ServiceIconContextProvider = ({ children }) => {

    const [serviceIcon, setServiceICon] = useState(
        (timeOfDay) => {
            if (timeOfDay === "Morning") {
                return "/images/icon_sun.png"
            }

            if (timeOfDay === "Afternoon") {
                return "/images/icon_afternoon.png"
            }

            if (timeOfDay === "Evening") {
                return "/images/icon_evening.png"
            }
        }
    )

    return (
        <ServiceIconContext.Provider value={[serviceIcon, setServiceICon]}>
            {children}
        </ServiceIconContext.Provider>
    )
}