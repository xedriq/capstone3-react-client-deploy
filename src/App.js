import React, { useState, createContext } from 'react';
import { BrowserRouter, Route } from 'react-router-dom'

// apollo client
import ApolloClient from 'apollo-boost'
import { ApolloProvider } from 'react-apollo'

//pages
import Home from './pages/Home'
import Signup from './pages/Signup';
import SignupUser from './pages/SignupUser';
import SignupPetType from './pages/SignupPetType'
import SignupPetBasics from './pages/SignupPetBasics';
import SignupPetDetails from './pages/SignupPetDetails';
import SignupVet from './pages/SignupVet';
import SignupConfirm from './pages/SignupConfirm'
import Dashboard from './pages/Dashboard';

// context
import { ColorContextProvider } from './ColorContext'
import { UserContext } from './UserContext'

import './App.css';
import BookServiceType from './pages/BookServiceType';
import BookDetails from './pages/BookDetails';
import BookConfirm from './pages/BookConfirm';
import Login from './pages/Login';
import UpdateUser from './pages/UpdateUser';
import UpdateService from './pages/UpdateService';

const client = new ApolloClient({ uri: 'https://xedriq-pawtastic.herokuapp.com/pawtastic' })

function App() {
  const [user ,setUser] = useState(null)

  return (
    <UserContext.Provider value={[user, setUser]}>

      <ApolloProvider client={client}>
        <ColorContextProvider >
          <BrowserRouter>
            <Route exact path="/" component={Home} />
            <Route exact path="/login" component={Login} />
            <Route exact path="/dashboard" component={Dashboard} />
            <Route exact path="/signup" component={Signup} />
            <Route path="/signup/user" component={SignupUser} />
            <Route exact path="/signup/pet-type" component={SignupPetType} />
            <Route exact path="/signup/pet-basics" component={SignupPetBasics} />
            <Route exact path="/signup/pet-details" component={SignupPetDetails} />
            <Route exact path="/signup/vet" component={SignupVet} />
            <Route exact path="/signup/confirm" component={SignupConfirm} />
            <Route exact path="/book/service-type/:petId" component={BookServiceType} />
            <Route exact path="/book/service-type" component={BookServiceType} />
            <Route path="/book/service-details" component={BookDetails} />
            <Route path="/book/service-confirm" component={BookConfirm} />
            <Route path="/update/user/:id" component={UpdateUser} />
            <Route path="/dashboard/:id" component={Dashboard} />
            <Route path="/update/service/:id" component={UpdateService} />

          </BrowserRouter>
        </ColorContextProvider>
      </ApolloProvider>
    </UserContext.Provider>
  );
}

export default App;
