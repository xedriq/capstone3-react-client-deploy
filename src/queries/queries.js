import { gql } from 'apollo-boost'

const getUserByEmailQuery = gql`
    query($email:String) {
    getUserByEmail(email:$email){
        id
        email
        password
    }
    }
`;

const getPetQuery = gql`
    query($id:String!){
    getPet(
        id:$id
    ){
        name
        pet_type
        birthday
        breed
        photo
        gender
        spayed_neutered
        weight
        favorites
        food
        others
    }
    }
`;

const getServicesQuery = gql`
    query{
    getServices{
        id
        name
        pet_id
        pet{
            id
            name
            photo
        }
        days
        times
        notes
        recurring
        start_date
        isCompleted
        sitter_id
    }
    }
`;

const getUserQuery = gql`
    query($id:String!){
        getUser(
            id:$id
        ){
            id
            first_name
            last_name
            phone
            alt_phone
            street_address
            city
            email
            user_type
            password
            pets{
                id
                name
                pet_type
                breed
                photo
                gender
            }
            # service_id
        }
    }
`;

const getPetsQuery = gql`
    query{
      getPets{
        id
        name
        pet_type
        birthday
        breed
        photo
        gender
        spayed_neutered
        weight
        favorites
        food
        others
        owner_id
      }
    }
`
const getServiceQuery = gql`
    query($id:String!) {
    getService(id:$id)
    {
        id
        name
        recurring
        start_date
        days
        times
        notes
        sitter_id
        pet_id
        isCompleted
        pet{
            id
            name
            pet_type
            birthday
            breed
            photo
            gender
            spayed_neutered
            weight
            favorites
            food
            others
        }
    }
    }
`;

const getUsersQuery = gql`
    query{
        getUsers{
            id
            email
            password
            first_name
            last_name
            phone
            alt_phone
            street_address
            city
            user_type
            password
        }
    }
`;
export {
    getUserByEmailQuery,
    getUserQuery,
    getUsersQuery,
    getPetQuery,
    getPetsQuery,
    getServicesQuery,
    getServiceQuery
}