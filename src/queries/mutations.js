import { gql } from 'apollo-boost'
const { GraphQLDateTime } = require('graphql-iso-date')

const customScalarResolver = {
    Date: GraphQLDateTime
}

const createUserMutation = gql`
    mutation(
        $email: String!
        $password: String!
    ){
    createUser(  
        email: $email
        password: $password
    )
    {
        id
        email
        password
        user_type
    }
    }
`;

const updateUserMutation = gql`
    mutation(
        $id:String!
        $first_name:String
        $last_name:String
        $email:String
        $password:String
        $phone:String
        $alt_phone: String
        $street_address:String
        $city:String
    ){
        updateUser(
            id:$id
            first_name:$first_name
            last_name:$last_name
            email:$email
            password:$password
            phone:$phone
            alt_phone: $alt_phone
            street_address:$street_address
            city:$city
        ){
            id
            email
            password
            first_name
            last_name
            phone
            alt_phone
            street_address
            city
            pets{
                id
                name
                pet_type
                birthday
                breed
                photo
                gender
                spayed_neutered
                weight
                food
                others
                }
            }
        }
`;


const createPetMutation = gql`
    mutation(
        $pet_type:String!
        $owner_id:String!
    ){
      createPet(pet_type:$pet_type owner_id:$owner_id){
        id
        pet_type
      }
    }
`;

const updatePetMutation = gql`
    mutation(
        $id: String
        $name: String
        $pet_type: String
        $breed: String
        $photo: String
        $gender: String
        $spayed_neutered: Boolean
        $weight: Int
        $birthday: Date
    ){
      updatePet(
        id:$id
        name:$name
        pet_type:$pet_type
        breed:$breed
        photo:$photo
        gender:$gender
        spayed_neutered:$spayed_neutered
        weight:$weight
        birthday:$birthday
      )
      {
        id
        name
        pet_type
        breed
        photo
        birthday
        gender
        spayed_neutered
        weight
      }
    }
`;

const logInUserMutation = gql`
    mutation($email:String! $password:String!){
    logInUser(email:$email password:$password){
        id
        first_name
        last_name
        phone
        alt_phone
        street_address
        city
        user_type
        services{
            id
            name
            recurring
            start_date
            days
            times
            notes
            sitter_id
        }
        pets{
            id
            name
            pet_type
            birthday
            breed
            photo
            gender
            spayed_neutered
            weight
            favorites
            food
            others
        }
    }
    }
`;

const createServiceMutation = gql`
    mutation(
        $pet_id: String!
        $name: String
        $recurring:Boolean
        $start_date: Date
        $days:[String]
        $times:[String]
        $notes:String
        $isCompleted:Boolean
    ){
    createService(
        pet_id:$pet_id
        name:$name
        recurring:$recurring
        start_date:$start_date
        days:$days
        times:$times
        notes:$notes
        isCompleted:$isCompleted
    ){
        id
        pet_id
        name
        recurring
        start_date
        days
        times
        notes
        isCompleted
    }
    }
`;

const deleteServiceMutation = gql`
    mutation($id:String!){
        deleteService(id:$id)
    }
`;

const updateServiceMutaion = gql`
    mutation(
        $id: String!
        $name: String
        $recurring: Boolean
        $start_date: String
        $days: [String]
        $times: [String]
        $notes: String
        $sitter_id: String
        $pet_id: String
        $isCompleted: Boolean
    ){
    updateService(
        id:$id
        name: $name
        recurring: $recurring
        start_date: $start_date
        days: $days
        times: $times
        notes: $notes
        sitter_id: $sitter_id
        pet_id: $pet_id
        isCompleted: $isCompleted
    ){
        id
        name 
        recurring
        start_date 
        days 
        times 
        notes 
        sitter_id 
        pet_id 
        isCompleted
    }
}
`
const deleteUserMutation = gql`
    mutation($id:String!){
    deleteUser(id:$id)
    }

`;
export {
    createUserMutation,
    updateUserMutation,
    deleteUserMutation,
    createPetMutation,
    updatePetMutation,
    logInUserMutation,
    createServiceMutation,
    deleteServiceMutation,
    updateServiceMutaion
}