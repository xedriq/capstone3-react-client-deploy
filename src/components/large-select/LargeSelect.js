import React, { useContext, useState } from 'react'
import styled from 'styled-components'
import { ColorContext } from '../../ColorContext'
import classNames from 'classnames'

// component
import SelectPhoto from './SelectPhoto'

const LargeSelect = (props) => {
    const [colors, setColors] = useContext(ColorContext)
    const [mouseOver, setMouseOver] = useState(false)

    const LargeSelectContainer = styled.div`
        background: white;
        padding:.5em;
        border-radius: 5px;
        display: flex;
        justify-content: space-between;
        width: 630px;
        margin: 0 auto;

        .selected {
            ${mouseOver && 'background: #EBD0CE;'}
            padding: .8em;
            border-radius: 5px;
        }
`
    const handleMouseOver = (e) => {
        console.log(e.target)
        setMouseOver(true)
    }
    const handleMouseOut = () => {
        setMouseOver(false)
    }


    return (
        // <LargeSelectContainer>
        //     <div className="selected" onMouseOver={handleMouseOver} onMouseOut={handleMouseOut}>
        //         <SelectPhoto image="/images/dog_01.png" optionName="Dog" size="150%" />
        //     </div>
        //     <div className="selected">
        //         <SelectPhoto image="/images/signup_image_1a.png" optionName="Cat" size="150%" />
        //     </div>
        //     <div className="selected">
        //         <SelectPhoto image="/images/thumbnail_image_02.png" optionName="Bird" size="100%" />
        //     </div>
        //     <div className="selected">
        //         <SelectPhoto image="/images/thumbnail_image_04.png" optionName="Others" size="100%" />
        //     </div>
        // </LargeSelectContainer >
        <form>
            <div className="form-group">
                <input className="mr-2" type="radio" id="dog" value="dog" name="petType" />
                <label htmlFor="dog">Dog</label>
            </div>
            <div className="form-group">
                <input className="mr-2" type="radio" id="cat" value="cat" name="petType" />
                <label htmlFor="cat">Cat</label>
            </div>
            <div className="form-group">
                <input className="mr-2" type="radio" id="bird" value="bird" name="petType" />
                <label htmlFor="bird">Bird</label>
            </div>
            <div className="form-group">
                <input className="mr-2" type="radio" id="others" value="others" name="petType" />
                <label htmlFor="others">Others</label>
            </div>
        </form>
    )
}

export default LargeSelect