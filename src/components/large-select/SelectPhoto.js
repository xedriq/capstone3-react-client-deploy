import React from 'react'
import styled from 'styled-components'

const SelectPhoto = (props) => {
    const PhotoContainer = styled.div`
        width: 110px;
        height: 110px;
        background-image: url("${props.image}");
        background-position: bottom center;
        background-size: ${props.size};
        border-radius: 50%;
        margin-bottom: .5em;
    `


    return (
        <React.Fragment>
            <PhotoContainer />
            <p className="m-0 text-center">{props.optionName}</p>
        </React.Fragment>
    )
}

export default SelectPhoto