import React, { useContext, useState } from 'react'
import styled from 'styled-components'
import { ColorContext } from '../../ColorContext'
import { Link } from 'react-router-dom'

//components
import ButtonCta from '../ButtonCta'


const DashboardSideBoxSection = styled.section`
    position: sticky;
    padding: 2em;
    min-height: 100vh;
    text-align: center;
    background: ${props => props.colors.primary};
    color: ${props => props.colors.greyLight};

    .account {
        position: absolute;
        bottom: .5%;
        left: 50%;
        transform: translate(-50%, 0%);
    }
`

const LogoIcon = styled.div`
    height: 50px;
    width: 50px;
    background-image: url("/images/logo_on_pink.png");
    background-size: cover;
    margin: 0 auto;
`

const DashboardSideBox = (props) => {
    // let user = JSON.parse(localStorage.getItem('user')) ? JSON.parse(localStorage.getItem('user')) : {}
    const [colors] = useContext(ColorContext)
    const [currentUser] = useState(JSON.parse(localStorage.getItem('user')))


    const showPets = () => {
        if (props.user.user_type === 'user') {
            return (
                <React.Fragment>
                    <div className="pets my-5">
                        <h4 className="my-4">Book a service for</h4>
                        {
                            props.user.pets.map(pet => (
                                <Link to={`/book/service-type/${pet.id}`} key={pet.id} >
                                    <p>{pet.name === null ? `Unnamed ${pet.pet_type}` : pet.name}</p>
                                </Link>
                            ))
                        }
                    </div>

                </React.Fragment>
            )
        } else {
            return <p>Hello Admin</p>
        }
    }


    return (
        <DashboardSideBoxSection colors={colors} className="sticky-top" >
            <Link to={localStorage.getItem('user') ? "/dashboard" : "/"}>
                <LogoIcon />
            </Link>

            <p className="text-center text-light mt-5">Hi {currentUser.first_name}!</p>
            {/*

            <div className="services">
                <h4 className="my-4">My Services</h4>
                <p>Scheduled</p>
                <p>Completed</p>
            </div>
            <Link to="/book/service-type">
                <ButtonCta
                    title="Book a Service"
                    bgColor={colors.tertiary}
                    colorHover={colors.tertiaryLight} />
            </Link>
            */}
            {
                showPets()
            }




            <div className="account">
                {props.user.user_type === "user" && (
                    <Link to='/signup/pet-type'>
                        <h5 className="my-5">Add Pet</h5>
                    </Link>
                )}
                <Link to={`/update/user/${props.user.id}`}>
                    <p>My Account</p>
                </Link>
                <Link to="/">
                    <p onClick={() => (localStorage.clear())}>Log Out</p>
                </Link>
            </div>
        </DashboardSideBoxSection >
    )
}

export default DashboardSideBox