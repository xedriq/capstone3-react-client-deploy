import React from 'react'
import styled from 'styled-components'

const ServiceAddressDiv = styled.div`
    position: relative;
    display: flex;
    align-items: center;
    padding: 1.5em;
    // border: 1px solid orange;

    p {
        line-height: 1;
    }

    .address {
        padding-left: 1em;
    }

    .editBtn {
        position: absolute;
        right: 30px;
    }
`

const IconDiv = styled.div`
    background-image: url("/images/icon_pinloc_round.png");
    background-size: cover;
    background-position: center;
    width: 50px;
    height: 50px;
    min-width: content;
`

const AddressDiv = styled.div`
    width: 75%;
    margin: 0 auto;
`

function ServiceAddress(props) {

    return (
        <ServiceAddressDiv>
            <IconDiv />
            <AddressDiv>
                <p>{props.address}</p>
                <p>{props.city} {props.zip_code}</p>
                <p>{props.phone}</p>
            </AddressDiv>
        </ServiceAddressDiv>
    )
}

export default ServiceAddress
