import React, { useState } from 'react'
import styled from 'styled-components'
import { uuid } from 'uuidv4'

import Moment from 'react-moment'

//components
import PetProfileCondense from '../pet-profile/PetProfileCondense'
import ServiceAddress from './ServiceAddress'
import NotesToSitter from './NotesToSitter'
import YourSitter from './YourSitter'

const IconDiv = styled.div`
    height: 30px;
    width: 30px;
    background-image: url(${props => props.imgIcon});
    background-size: cover;
    background-position: center
    // display: inline-block;
    margin-right: 1em;
`

const ServiceDetailSection = styled.section`
    li {
        list-style: none;
        display: flex;
        align-items: center;
    }
`

const ServiceDetail = ({ service, handleDelete }) => {
    const [currentUser, setCurrentUser] = useState(JSON.parse(localStorage.getItem('user')))

    const handleTODIcon = (time) => {
        // timeOfDay = timeOfDay.toLowerCase()
        if (time === "morning") {
            return "/images/icon_sun.png"
        }

        if (time === "afternoon") {
            return "/images/icon_afternoon.png"
        }

        if (time === "evening") {
            return "/images/icon_evening.png"
        }
    }

    const getServiceIcon = (serviceName) => {
        if (serviceName === "house sitting") {
            return "/images/icon_house_sitting.png"
        }

        if (serviceName === "dog walk") {
            return "/images/icon_dog_walk.png"
        }

        if (serviceName === "drop-in visit") {
            return "/images/icon_drop_in_visit.png"
        }
    }

    return (
        <ServiceDetailSection className="sticky-top">
            <div className="row m-0 p-3 align-items-center">
                <div className="col-6 p-0 m-0">
                    <Moment date={service.start_date} format="dddd, MMMM Do" />
                </div>
                <div className="col-6 p-0 m-0">
                    <li><IconDiv imgIcon={getServiceIcon(service.name)} /> <span className="iconText">{service.name}</span></li>
                    {
                        service.times.map(time => {
                            return <li key={uuid()} ><IconDiv imgIcon={handleTODIcon(time)} /> <span className="iconText">{time}</span></li>
                        })
                    }
                </div>
            </div>

            <hr className="p-0 m-0" />

            <PetProfileCondense isCompleted={service.isCompleted} petName={service.pet.name} photo={service.pet.photo} handleDelete={handleDelete} />

            <hr className="p-0 m-0" />

            <ServiceAddress address={currentUser.street_address} city={currentUser.city} />

            <hr className="p-0 m-0" />
            <NotesToSitter />

            <hr className="p-0 m-0" />

            <YourSitter />
        </ServiceDetailSection>
    )
}

export default ServiceDetail