import React, { useState, useContext, useRouter } from 'react'
import styled from 'styled-components'
import { ColorContext } from '../../ColorContext'
import { uuid } from 'uuidv4'

import Moment from 'react-moment';

const ServiceListItem = ({ date, day, timeOfDay, serviceType, petImage, service }) => {
    const [colors] = useContext(ColorContext)

    const ServiceListItemDiv = styled.div`
        background: ${colors.tertiaryLight}
        border: 1px ${colors.tertiary};
        border-style: solid none solid none;
        padding: 1em 3em;
        cursor: pointer;
        
        span {
            font-weight: bolder;
        }

        p {
            display: inline-block;
        }

        li {
            list-style: none;
            display: flex;
            align-items:center;
        }

        .iconText {
            font-weight: normal;
            
        }

        &:hover {
            background: ${colors.grey};
        }

        &:active {
            background: ${colors.greyLight};
        }
        
    `

    const IconDiv = styled.div`
        height: 20px;
        width: 20px;
        background-image: url("${props => props.imgIcon}");
        background-size: cover;
        display: inline-block;
        margin-right: 1em;
    `

    const PetImageDiv = styled.div`
        height: 100px;
        width: 100px;
        border-radius: 5px;
        background-image: url("${props => props.petPhoto}");
        background-size: cover;
        background-position: center;
        float: right;
        border: 1px solid ${colors.secondaryLight}
    `
    const handleTODIcon = (timeOfDay) => {
        // timeOfDay = timeOfDay.toLowerCase()

        if (timeOfDay === "morning") {
            return "/images/icon_sun.png"
        }

        if (timeOfDay === "afternoon") {
            return "/images/icon_afternoon.png"
        }

        if (timeOfDay === "evening") {
            return "/images/icon_evening.png"
        }
    }
    
    const handleServiceIcon = (service) => {
        if(service === "dog walk") {
            return "/images/icon_dog_walk.png"
        }

        if(service === "drop-in visit") {
            return "/images/icon_drop_in_visit.png"
        }

        if(service === "house sitting") {
            return "/images/icon_house_sitting.png"
        }
    }


    return (
        <ServiceListItemDiv>
            <div className="row m-0 p-0">
                <div className="col-8 p-0">
                    <h6><span><Moment format="dddd" date={service.start_date} /></span> <Moment format="MMMM Do" date={service.start_date} /> </h6>
                    <li>
                        <IconDiv imgIcon={handleServiceIcon(service.name)} /><span className="iconText"> {service.name}</span></li>
                    {
                        service.times.map(time => {
                            return <li key={uuid()}><IconDiv imgIcon={handleTODIcon(time)} /> <span className="iconText"> {time}</span></li>
                        })
                    }
                </div>
                <div className="col-4 p-0">
                    <PetImageDiv petPhoto={service.pet.photo} />
                </div>
            </div>
        </ServiceListItemDiv>
    )
}

export default ServiceListItem