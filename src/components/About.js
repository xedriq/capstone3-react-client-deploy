import React, { useContext } from 'react'
import { Link } from 'react-router-dom'

//styled-components
import styled from 'styled-components'

//context
import { ColorContext } from '../ColorContext'

// components
import ButtonCta from './ButtonCta'
import Thumbnail from './Thumbnail'
import Testimonial from './Testimonial'
import PricingAndPlan from './PricingAndPlan'


const ImgContainer = styled.div`
    background-image: url("/images/bg.png");
    background-size: cover;
    background-position: bottom center;
    height: 600px;

    width: 100%;
`


const About = () => {
    const [colors] = useContext(ColorContext)

    const About2 = styled.section`
        background: ${colors.tertiaryLight};
        margin: 0;
        padding: 0;

        .about-2-supers {
            padding-left: 5em;
        }
    `
    const About3 = styled.section`
        background: ${colors.primary};
        padding: 5em 0;
        margin: 0;
    `

    return (
        < React.Fragment >
            <section className="py-5 px-0 m-0" id="about">
                <div className="container">
                    <div className="row my-5 mx-0">
                        <div className="col-12 col-md-6 align-self-center">
                            <h2 className="mb-4">Expert care for your
                                furry, feathery, or
                                scaley friend
								</h2>
                            <p className="lead mb-5">We know how stressful it is to leave your pets at
                                home alone. We’re a team of experienced
                                animal caregivers, well connected to local
                                veterinarians. Trust to us to love them like our
                                own, and to keep them safe and happy till
                                you’re home.
								</p>
                            <Link to={localStorage.getItem('user') ? "/dashboard" : "/login"}>
                                <ButtonCta title="Schedule a visit" color={colors.greyLight} bgColor={colors.primary} colorHover={colors.primaryLight} type="button" />
                            </Link>

                        </div>
                        <div className="col-12 col-md-6">
                            <div className="row justify-content-end">
                                <div className="d-flex">
                                    <Thumbnail name="Ced" photo="/images/thumbnail_image_01.png" />
                                    <Thumbnail name="Liz" photo="/images/thumbnail_image_02.png" />
                                </div>
                            </div>
                            <div className="row justify-content-end">
                                <div className="d-md-flex">
                                    <Thumbnail name="Thine" photo="/images/thumbnail_image_03.png" />
                                    <Thumbnail name="Mac" photo="/images/thumbnail_image_04.png" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

            <About2>
                <div className="row align-items-center">
                    <div className="col-12 col-md-6">
                        <ImgContainer />

                    </div>

                    <div className="about-2-supers col-12 col-md-6">
                        <h2 className="mb-4">Services tailored</h2>
                        <p className="lead mb-5 w-75">Schedule one-off or recurring home
                            visits. An experienced member of our
                            team will spend time with your pet, feed
                            them, change cat litter trays, take the
                            dog for a walk, and anything else you
                            need.
						</p>
                        <Link to={localStorage.getItem('user') ? "/dashboard" : "/login"}>
                            <ButtonCta title="Schedule a visit" color={colors.greyLight} bgColor={colors.primary} colorHover={colors.primaryLight} type="button" />
                        </Link>
                    </div>
                </div>
            </About2>

            <About3 id="reviews">
                <div className="container">
                    <h2 align="center" style={{ color: "white" }}>Pets (and their humans) love us</h2>
                    <div className="row mt-5">
                        <div className="col-12 col-md-6">
                            <Testimonial
                                photo="/images/testi_lindsay.png"
                                name="Earl C"
                                testi="Pawtastic is awesome! They are passionate about pets and employ trustworthy, dependable staff. We love them!"
                            />
                        </div>
                        <div className="col-12 col-md-6">
                            <Testimonial
                                photo="/images/testi_floof.png"
                                name="MC C"
                                testi="I’m a repeat customer because of their amazing care for our two cats when we travel. I can relax because I know they’re there!"
                            />
                        </div>
                    </div>
                    <div className="row mt-4">
                        <div className="col-12 col-md-6">
                            <Testimonial
                                photo="/images/testi_ginger.png"
                                name="Meg F"
                                testi="I use them for mid day walks and our babies are so happy with the exercise and love during the day. We see the difference!"
                            />
                        </div>
                        <div className="col-12 col-md-6">
                            <Testimonial
                                photo="/images/testi_ned.png"
                                name="Jackie B"
                                testi="I just returned from two weeks away to a sociable, calm cat and no drama. Thanks for a great job, Pawtastic!"
                            />
                        </div>
                    </div>
                </div>
                <div className="my-5" align="center">
                    <ButtonCta bgColor="#EBD0CE" colorHover="#F7ECEB" title="Read all reviews" />
                </div>
            </About3>

            <section className="about-4 py-5 container my-5 " id="services">
                <h2 align="center" className="mb-5 pb-5">Affordable options,
                    tailored to your needs
				</h2>
                <div className="row p-0 m-0">
                    <div className="col-12 col-md-4">
                        <PricingAndPlan
                            icon="/images/icon_dog_walk.png"
                            price="800"
                            priceDesc="per hour walk"
                            name="Dog Walk"
                            description="We’ll take your pup for a 30 minute walk and make sure he or she has fresh food and water."
                        />

                    </div>
                    <div className="col-12 col-md-4">
                        <PricingAndPlan
                            icon="/images/icon_drop_in_visit.png"
                            price="800"
                            priceDesc="per hour visit"
                            name="Drop In Visit"
                            description="We’ll stop by to snuggle, feed, and play with your pets in the comfort of their own home."
                        />
                    </div>
                    <div className="col-12 col-md-4">
                        <PricingAndPlan
                            icon="/images/icon_house_sitting.png"
                            price="2000"
                            priceDesc="per night"
                            name="House Sitting"
                            description="We’ll stay overnight with your pets to make sure they have round-the-clock love."
                        />
                    </div>
                    <div className="text-center mx-auto mt-5">
                        <Link to={localStorage.getItem('user') ? "/dashboard" : "/login"}>
                            <ButtonCta title="Schedule a visit" color="#F0F1F7" bgColor="#545871" colorHover="#9597A6" type="button" />
                        </Link>
                    </div>
                </div>
            </section>
        </React.Fragment >
    )
}

export default About