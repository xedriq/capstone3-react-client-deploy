import React from 'react'
import styled from 'styled-components'

const Thumbnail = (props) => {
	const ThumbnailDiv = styled.div`
		position: relative;
		height: 180px;
		width: 180px;
		min-width: 180px;
		border-radius: 10px;
		background-image: url("${props.photo}");
		background-size: cover;
		background-position: bottom center;

		.name {
			position: absolute;
			bottom: 0;
			color: #F0F1F7;
		}

	`

	return (
		<ThumbnailDiv className="thumbnail p-4 m-3">
			<p className="name">{props.name}</p>
		</ThumbnailDiv>
	)
}

export default Thumbnail