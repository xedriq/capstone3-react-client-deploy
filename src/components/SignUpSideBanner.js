import React, { useState } from 'react'
import { Link } from 'react-router-dom'
import styled from 'styled-components'

const SignUpSideBannerContainer = styled.div`
        background-image: url(${props => props.photo});
        background-size: cover;
        background-repeat: no repeat;
        background-position: bottom center;
        height: 100vh;
        width: 100%;
        color: white;
        padding: 4em;

        .logo {
            height: 50px;
            width: 50px;
            background-image: url("/images/logo_on_pink.png"); 
            background-size: cover;
        }
        
        a:link,
        a:visited,
        a:hover,
        a:active {
            text-decoration: none;
            color: white;
        }
    `

const SignUpSideBanner = ({ title, photo, content }) => {
    const [user, setUser] = useState(JSON.parse(localStorage.getItem('user')))
    return (
        <React.Fragment>
            <SignUpSideBannerContainer photo={photo}>
                <Link to={user ? '/dashboard' : '/'}>
                    <div className="d-flex align-items-center justify-content-center mb-5">
                        <div className="logo mr-2"></div>
                        <h3>Pawtastic</h3>
                    </div>
                </Link>
                <h4 className="mb-4 text-center">{title}</h4>
                <p className="text-center">{content}</p>
            </SignUpSideBannerContainer>
        </React.Fragment>
    )
}

export default SignUpSideBanner