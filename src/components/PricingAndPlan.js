import React from 'react'
import styled from 'styled-components'

const PricingAndPlan = (props) => {

    const PricingPlan = styled.div`
        position: relative;
        border: 1px solid #DADBE6;
        border-radius: 5px;
        text-align: center;
        overflow: hidden;
        background:white;

        .pp-content {
            text-align: center;
            width: 75%;
            padding: 4em 0 2em 0;
        }

         .price {
            height: 100%;
            padding: 2em 0;
            background: #DADBE6;
        }
    `

    const PPIcon = styled.div`
        background-image: url(${props.icon});
        background-size: cover;
        height: 80px;
        width: 80px;
        border: 1px solid #E6DBDA;
        border-radius: 50%;

        position: absolute;
        top: 0%;
        left: 50%;
        transform: translate(-50%, -50%);
        z-index: 10;
    `

    return (
        <React.Fragment>
            <PPIcon />
            <PricingPlan className="pricing-plan">
                <div className="pp-content mx-auto">
                    <h2 className="mb-3">{props.name}</h2>
                    <p>{props.description}</p>
                </div>
                <div className="price">
                    <h4>P {props.price}</h4>
                    <p>{props.priceDesc}</p>
                </div>
            </PricingPlan>
        </React.Fragment>
    )
}

export default PricingAndPlan