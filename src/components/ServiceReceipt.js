import React from 'react'
import styled from 'styled-components'
import { uuid } from 'uuidv4'

// components


// moment
import Moment from 'react-moment'

const ServiceReceiptDiv = styled.div`
    position: relative;
    background: white;
    border-radius: 5px;
    width: ${props => props.width};
    margin: 0 auto;
    overflow: hidden;
    padding: 1em 2em;

    li {
        list-style: none;
    }
`

export const PetImageRound = styled.div`
    background-image: url("${props => props.imgUrl}");
    background-size: cover;
    height: 70px;
    width: 70px;
    border: 1px solid lightgrey;
    border-radius: 50%;
`

const HeaderDiv = styled.div`
    display: flex;
    justify-content: space-between;
    align-items: center;
    width:80%;
    margin: 0 auto;
    padding: 1em 0;
`

function ServiceReceipt({ width, service, pet }) {
    return (
        <ServiceReceiptDiv width={width}>
            <div className="row m-0 p-0">
                <div className="col-12 m-0 p-0">
                    <HeaderDiv>
                        <p>{service.serviceName} for {pet.name}</p>
                        <PetImageRound imgUrl={pet.photo} />
                    </HeaderDiv>
                </div>
            </div>
            <div className="row m-0 p-0">
                <div className="col-12 m-0 p-0">
                    <ul>
                        <li>Service type <span className="badge badge-pill badge-info">{service.serviceName}</span></li>
                        <li>Start Date <span className="badge badge-pill badge-info">
                            <Moment date={service.startDate} format="dddd MMMM DD, YYYY" />
                        </span></li>
                        <li>Service Time {
                            service.times.map(time => (
                                <span key={uuid()} className="badge badge-pill badge-info">{time}</span>
                            ))
                        } </li>
                        <li>Recurring {
                            service.days.map(day => (
                                <span key={uuid()} className="badge badge-pill badge-info">{day}</span>
                            ))
                        }

                        </li>
                    </ul>
                </div>
            </div>
        </ServiceReceiptDiv>
    )
}

export default ServiceReceipt
