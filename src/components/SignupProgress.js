import React from 'react'
import classNames from 'classnames'
import styled from 'styled-components'

const RoundsContainer = styled.div`
        height: 250px;
        .round {
            width: 20px;
            height: 20px;
            background: #9597A6;
            border-radius: 50%;
            display: inline-block;
            margin-right: 1.5em;
        }

        .rounds {
            height: 100%;
        }

        .round-name {
            display: flex;
        }

        .round-active {
            width: 30px;
            height: 30px;
            border: 5px solid #fff;
            background: #545871;
            margin-left: -5px;
        }
    `

const SignupProgress = (props) => {

    return (
        <React.Fragment>
            <RoundsContainer className="d-flex justify-content-center pt-2">
                <div className="rounds d-flex flex-column justify-content-between align-items-start">
                    <div className="round-name">
                        <div className={classNames("round", { "round-active": props === "user" })}>
                        </div>
                        <p>Human Profile</p>
                    </div>
                    <div className="round-name">
                        <div className={classNames("round", { "round-active": props === "pet type" || props === "pet basics" })}>
                        </div>
                        <p>Pet Basics</p>
                    </div>
                    <div className="round-name">
                        <div className={classNames("round", { "round-active": props === "pet details" })}>
                        </div>
                        <p>Pet Details</p>
                    </div>
                    <div className="round-name">
                        <div className={classNames("round", { "round-active": props === "confirm" })}>
                        </div>
                        <p>Confirm</p>
                    </div>
                </div>
            </RoundsContainer>
        </React.Fragment >
    )
}

export default SignupProgress