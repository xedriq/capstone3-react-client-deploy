import React, { useContext } from 'react'
import { Link } from 'react-router-dom'

// styled-components
import styled from 'styled-components'

//context
import { ColorContext } from '../ColorContext'

//components
import ButtonCta from './ButtonCta'

// css
import '../App.css'

const HeaderSection = styled.section`
    color: white;
    height: 95vh;
    background-image: url("/images/hero-image.png");
    background-size: cover;
    background-position: center bottom;
    background-repeat: no-repeat;

    .menu li {
        list-style: none;
        margin: 0 1em;
        display: inline;
        min-width: content;
    }

    .logo {
        background-image: url("/images/logo.png");
        background-size: cover;
        height:60px;
        width:60px;
        margin-right: 10px;
    }

    h1 {
        margin-top: -10px;
    }

    a:link,
    a:visited,
    a:hover,
    a:active {
        text-decoration: none;
        color: white;
    }
`

const Header = () => {
    const [colors] = useContext(ColorContext)

    return (
        <HeaderSection>
            <div className="container">
                <div className="brand-name d-flex align-items-centeralign-items-center pt-5">
                    <div className="logo"></div><h1 className="mt-2">Pawtastic</h1>
                </div>

                <div className="row py-5 m-0">
                    <div className="col-12 col-md-5 offset-md-7 align-items-center">
                        <ul className="menu d-flex">
                            <li>
                                <Link to="#about">
                                    About us
                                </Link>
                            </li>
                            <li>
                                <Link to="#reviews">
                                    Reviews
                                </Link>
                            </li>
                            <li>
                                <Link to="#services">
                                    Services
                                </Link>
                            </li>
                            <li>
                                <Link to="/signup">
                                    Sign Up
                                </Link>
                            </li>
                        </ul>
                        <div className="header-container d-flex flex-column align-items-center mt-5">
                            <h1 className="mb-5">We care for your furry
								little love ones while you're away.</h1>
                            <Link to={localStorage.getItem('user') ? "/dashboard" : "/login"}>
                                <ButtonCta 
                                    title="Schedule a visit" 
                                    bgColor={colors.tertiary} 
                                    colorHover={colors.tertiaryLight} />
                            </Link>
                        </div>
                    </div>
                </div>
            </div>
        </HeaderSection>
    )
}

export default Header