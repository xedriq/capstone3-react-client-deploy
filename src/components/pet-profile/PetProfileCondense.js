import React, { useState } from 'react'
import styled from 'styled-components'
import { Link, useParams } from 'react-router-dom'

// components
import ImageCircle from './ImageCircle'
import DetailBox from './DetailBox'

function PetProfileCondense(props) {

    const [currentUser, setCurrentUser] = useState(JSON.parse(localStorage.getItem('user')))

    const handleDelete = (e) => {
        props.handleDelete()
    }

    const PetProfileCondenseDiv = styled.div`
        position: relative;
        padding: 1em;
        
        .topContainer {
            display: flex;
            align-items: center;
            margin-bottom: 1em;
        }

        .petName {
            line-height: .6;
            padding-left: 1em;
        }


        buttonsContainer {

        }

    `
    let id = useParams()
    return (
        <PetProfileCondenseDiv>
            <div className="topContainer">
                <ImageCircle petPhoto={props.photo} attrib={{
                    position: "relative",
                    top: "40px",
                    left: "40px",
                }} />

                <div className="petName">
                    <p>Service for</p>
                    <p>{props.petName}</p>
                </div>
                <div className="buttonsContainer ml-auto">
                    {
                        currentUser.user_type === "user" && !props.isCompleted ? (

                    <button
                        className="btn btn-danger mx-2"
                        onClick={handleDelete}
                    >Delete</button>
                        ): ''
                    }

                    {currentUser.user_type === "admin"
                        ? (
                            <Link to={`/update/service/${id.id}`}>
                                <button className="btn btn-info">Edit</button>
                            </Link>
                        )
                        : ''
                    }
                </div>

            </div>
            {/*
            <DetailBox />
            */}

        </PetProfileCondenseDiv>
    )
}

export default PetProfileCondense
