import React, { useContext } from 'react'
import styled from 'styled-components'
import { ColorContext } from '../../ColorContext'


//components
import ImageCircle from './ImageCircle'
import DetailBox from './DetailBox'

const PetProfileDiv = styled.div`
    position: relative;
    border: 1px solid lightgrey;
    border-radius: 5px;
    background: #fff;
    padding: 3.5em 1em 1em 1em;
    text-align:center;

    span {
        background: ${props => props.color};
    }

`

const PetProfile = ({ petData }) => {
    const [colors] = useContext(ColorContext)

    return (
        <PetProfileDiv color={colors.tertiaryLight}>
            <ImageCircle
                petPhoto={petData.photo}
                attrib={{
                    position: "absolute",
                    top: 0,
                    left: "50%"
                }}
            />
            <h3>{petData.name}</h3>
            <p>{petData.breed}</p>
            <hr className="w-75 mx-auto" />
            {/* <h6>Favorite Things</h6> */}
            {/* <div className="badges d-flex justify-content-around w-75 mx-auto my-4 p-0">
                <span className="badge badge-pill">Barking</span>
                <span className="badge badge-pill">Snuggling</span>
                <span className="badge badge-pill">Giving kisses</span>
                <span className="badge badge-pill">Walks</span>
                <span className="badge badge-pill">Treats</span>
            </div> */}
            <DetailBox petData={petData} />
        </PetProfileDiv>
    )
}

export default PetProfile