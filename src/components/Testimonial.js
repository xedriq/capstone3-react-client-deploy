import React from 'react'
import styled from 'styled-components'

import Thumbnail from './Thumbnail'

const Testimonial = (props) => {
    const TestimonialDiv = styled.div`
        background: #F0F1F7;
        border-radius: 10px;

    `

    return (
        <React.Fragment>
            <TestimonialDiv className="d-flex justify-content-between">
                <Thumbnail photo={props.photo} />
                <div className="row align-items-center">
                    <div className="col-12 px-4">
                        <h4>{props.name}</h4>
                        <small>"{props.testi}"</small>
                    </div>
                </div>
            </TestimonialDiv>
        </React.Fragment>
    )
}

export default Testimonial