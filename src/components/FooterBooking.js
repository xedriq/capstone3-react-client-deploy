import React, { useContext } from 'react'
import styled from 'styled-components'
import { Link } from 'react-router-dom'

//context
import { ColorContext } from '../ColorContext'

// components
import ButtonCta from './ButtonCta'

const FooterBookingDiv = styled.div`
    display: flex;
    justify-content: space-around;
`

function FooterBooking({ backTo, nextTo, nextTitle, nextBtnType }) {
    const [colors] = useContext(ColorContext)

    return (
        <FooterBookingDiv>
            <Link to={backTo}>
                <ButtonCta
                    type="button"
                    title="Back"
                    color={colors.greyLight}
                    bgColor={colors.primary}
                    colorHover={colors.primaryLight}
                />
            </Link>

            <ButtonCta
                type={nextBtnType}
                title={nextTitle}
                color={colors.greyLight}
                bgColor={colors.primary}
                colorHover={colors.primaryLight}
            />

        </FooterBookingDiv>
    )
}

export default FooterBooking
