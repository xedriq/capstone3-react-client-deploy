import React, { useState, useEffect } from 'react'
import styled from 'styled-components'
import { graphql } from 'react-apollo'
import { Redirect } from 'react-router-dom'

// components
import ServiceReceipt from '../components/ServiceReceipt'
import SignUpSideBanner from '../components/SignUpSideBanner'
import FooterBooking from '../components/FooterBooking'

// styled components
import { FooterDiv } from './BookServiceType'

//mutations - queries
import { createServiceMutation } from '../queries/mutations'

// loadash
import { flowRight as compose } from 'lodash'

import Swal from "sweetalert2";

// query
import { getPetQuery, getServicesQuery } from '../queries/queries'


const BookConfirmDiv = styled.div`

`

function BookConfirm(props) {
    // const [pet, setPet] = useState(JSON.parse(localStorage.getItem('pet')))
    const [service] = useState(JSON.parse(localStorage.getItem('service')))
    const [next, setNext] = useState(false)

    let pet = props.getPetQuery.getPet ? props.getPetQuery.getPet : {}

    useEffect(() => {
        console.log(service)
    })
    const handleSubmit = (e) => {
        e.preventDefault()

        props.createServiceMutation({
            variables: {
                ...service,
                pet_id: localStorage.getItem('petId'),
                name: service.serviceName,
                start_date: service.startDate
            },
            refetchQueries: [
                {
                    query: getServicesQuery
                }
            ]
        }).then(res => {
            console.log(res)
            Swal.fire({
                icon: 'success',
                title: `Service is set for ${pet.name}!`
            })
        }).then(() => {
            setNext(true)
        }).catch(error => {
            console.log(error.message)
            Swal.fire({
                icon: "error",
                title: String(error.message)
            });
        })
    }

    if (next) {
        return <Redirect to="/dashboard" />
    }

    return (
        <BookConfirmDiv>
            <div className="row p-0 m-0">
                <div className="col-12 col-md-4 p-0 m-0">
                    <SignUpSideBanner content="why our service?" photo='/images/cat_01.png' />
                </div>
                <div className="col-12 col-md-8 p-0 m-0">
                    <h3 className="text-center">Almost there, let’s just confirm all the details.</h3>
                    <ServiceReceipt width="80%" service={service} pet={pet} />
                    <form onSubmit={handleSubmit}>
                        <FooterDiv>
                            <FooterBooking backTo="/book/service-details" nextTo="/dashboard" nextTitle="Schedule Service" />
                        </FooterDiv>
                    </form>
                </div>
            </div>
        </BookConfirmDiv>
    )
}

export default compose(
    graphql(createServiceMutation, { name: 'createServiceMutation' }),
    graphql(getServicesQuery, { name: 'getServicesQuery' }),
    graphql(getPetQuery, {
        options: () => {
            return { variables: { id: localStorage.getItem('petId') } }
        },

        name: 'getPetQuery'
    })
)(BookConfirm)
