import React, { useState, useContext } from 'react'
import styled from 'styled-components'
import { Link, Redirect } from 'react-router-dom'
import { graphql } from 'react-apollo'

// components
import SignUpSideBanner from '../components/SignUpSideBanner'
import ButtonCta from '../components/ButtonCta'

//queries - mutations
import { logInUserMutation } from '../queries/mutations'

import Swal from "sweetalert2";

// loadash
import { flowRight as compose } from 'lodash'

//context
import { ColorContext } from '../ColorContext'
import { UserContext } from '../UserContext'

const LoginDiv = styled.div`

`

function Login(props) {
    const [user, setUser] = useContext(UserContext)
    const [email, setEmail] = useState('')
    const [password, setPassword] = useState('')
    const [isLoggedIn, setIsLoggedIn] = useState(false)
    const [colors] = useContext(ColorContext)
    // const [loggedInUser, setLoggedInUser] = useState({})

    // useEffect(() => {
    //     console.log(isLoggedIn, email, password)
    // }, [loggedInUser])
    console.log(user)
    const handleChange = (e) => {
        let { id, value } = e.target

        if (id === 'email') {
            setEmail(value)
        }

        if (id === "password") {
            setPassword(value)
        }
    }

    const handleSubmit = (e) => {
        e.preventDefault()

        props.logInUserMutation({
            variables: {
                email,
                password
            }
        }).then(res => {
            localStorage.setItem('user', JSON.stringify(res.data.logInUser))
            setUser(res.data.logInUser)
            Swal.fire({
                icon: 'success',
                title: 'You are now logged in!'
            })
        }).then(() => {
            setIsLoggedIn(true)
        }).catch(error => {
            console.log(error.message)
            Swal.fire({
                icon: "error",
                title: String(error.message)
            });
        })

    }

    if (isLoggedIn) {
        localStorage.setItem('isLoggedIn', JSON.stringify(isLoggedIn))
        return <Redirect to='/dashboard' />
    }



    return (
        <LoginDiv>
            <div className="row m-0 p-0">
                <div className="col-12 col-md-4 m-0 p-0">
                    <SignUpSideBanner content="Welcome pet lovers!" photo="/images/pet_01_01.png" />
                </div>
                <div className="col-12 col-md-8 m-0 p-0 align-self-center">
                    <h3 className="text-center">Let's look for a pet sitter.</h3>
                    <form onSubmit={handleSubmit} className="w-75 mx-auto">
                        <div className="form-group">
                            <label htmlFor="email">Email</label>
                            <input
                                type="email"
                                id="email"
                                className="form-control"
                                onChange={handleChange}
                                value={email} />
                        </div>
                        <div className="form-group">
                            <label htmlFor="password">Password</label>
                            <input
                                type="password"
                                id="password"
                                className="form-control"
                                onChange={handleChange}
                                value={password} />
                        </div>
                        <div className="text-center py-4">
                            <ButtonCta
                                title="Log In"
                                bgColor={colors.primary}
                                color="white"
                                colorHover={colors.primaryLight}
                            />
                        </div>
                    </form>
                    <p className="text-center">
                        Not account yet? Signup <Link to="/signup">here.</Link>
                    </p>
                </div>
            </div>
        </LoginDiv>
    )
}

export default compose(
    graphql(logInUserMutation, { name: 'logInUserMutation' })
)(Login)
