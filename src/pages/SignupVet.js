import React, { useContext } from 'react'
import { Link } from 'react-router-dom'
import styled from 'styled-components'
import { ColorContext } from '../ColorContext'

// components
import SignupProgress from '../components/SignupProgress'
import SignUpSideBanner from '../components/SignUpSideBanner'
import LargeSelect from '../components/large-select/LargeSelect'
import ButtonCta from '../components/ButtonCta'

const SignupVet = (props) => {
    const [colors, setColors] = useContext(ColorContext)
    console.log(props)
    const SaveExitContainer = styled.div`
        position: relative;
        
        a:link,
        a:visited,
        a:hover,
        a:active {
            text-decoration: none;
            color: white;
        }

        .save-exit {
            position: absolute;
            bottom: 40px;
            right: 50%;
            padding: 0;
            transform: translate(50%, -50%);
        }
    `

    const PetType = styled.div`
        background: ${colors.tertiaryLight};
    `

    return (
        <React.Fragment>
            <PetType>
                <div className="row">
                    <SaveExitContainer className="col-12 col-md-4 p-0">
                        <SignUpSideBanner
                            photo="/images/pug_01.png"
                            content={SignupProgress("pet details")}
                        />

                        <Link to='/' >
                            <a className="save-exit"> Save and Exit</a>
                        </Link>

                    </SaveExitContainer>
                    <div className="col-12 col-md-8 p-0 text-center">
                        <h2 className="w-75 mx-auto  my-5">Got a preferred vet?
                            Let us know.</h2>

                        <form enctype="multipart/form-data" className="w-75 mx-auto text-left">
                            <div className="row">
                                <div className="col-12 col-md-6">
                                    <div className="form-group">
                                        <label htmlFor="vetName">Veterinarian's Name</label>
                                        <input type="text" className="form-control" id="vetName" />
                                    </div>

                                </div>
                                <div className="col-12 col-md-6">
                                    <div className="form-group">
                                        <label htmlFor="vetPhone">Phone</label>
                                        <input type="number" id="vetPhone" className="form-control" />
                                    </div>
                                </div>
                            </div>

                            <div className="row">
                                <div className="col-12">
                                    <div className="form-group">
                                        <label htmlFor="vetStreetAddress">Street Address</label>
                                        <input type="text" className="form-control" id="vetStreetAddress" />
                                    </div>
                                </div>
                            </div>
                            <div className="row">
                                <div className="col-12 col-md-6">
                                    <div className="form-group">
                                        <label htmlFor="vetCity">City</label>
                                        <input type="text" id="vetCity" className="form-control" />
                                    </div>
                                </div>
                                <div className="col-12 col-md-6">
                                    <div className="form-group">
                                        <label htmlFor="vetZipCode">Zip Code/Postal Code</label>
                                        <input type="number" id="vetZipCode" className="form-control" />
                                    </div>
                                </div>
                            </div>

                        </form>
                        <p className="w-75 mx-auto">We’ve got vets on-call 24/7 in case of emergencies, so don’t worry if you don’t have a preferred doctor.</p>

                        <div className="row mt-5">
                            <div className="col-12 col-8">
                                <div className="signup-footer d-md-flex justify-content-md-between align-items-md-center w-75 mx-auto">
                                    <Link to="/signup/pet-details">
                                        <ButtonCta
                                            title="Back"
                                            color={colors.greyLight}
                                            bgColor={colors.primary}
                                            colorHover={colors.primaryLight}
                                        />
                                    </Link>

                                    <Link to="/signup/confirm">
                                        Skip
                                    </Link>

                                    <Link to="/signup/confirm">
                                        <ButtonCta
                                            title="Next"
                                            color={colors.primary}
                                            bgColor={colors.grey}
                                            colorHover={colors.greyLight}
                                        />
                                    </Link>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </PetType>
        </React.Fragment>
    )
}

export default SignupVet