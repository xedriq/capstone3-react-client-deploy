import React, { useState, useEffect, useContext } from 'react'
import styled from 'styled-components'
import { Redirect } from 'react-router-dom'

import { graphql } from 'react-apollo'
// loadash
import { flowRight as compose } from 'lodash'

import Swal from "sweetalert2";

//query
import { getServiceQuery, getUsersQuery } from '../queries/queries'

// mutations
import { updateServiceMutaion } from '../queries/mutations'

// components
import DashboardSideBox from '../components/dashboard/DashboardSideBox'
import PetProfile from '../components/pet-profile/PetProfile'
import ButtonCta from '../components/ButtonCta'

import { ColorContext } from '../ColorContext'

const UpdateServiceSection = styled.section`

`
const PetProfileDiv = styled.div`
    width: 80%;
    margin: 0 auto;
`


function UpdateService(props) {
    console.log(props)
    const [currentUser] = useState(JSON.parse(localStorage.getItem('user')))
    const [sitter, setSitter] = useState('')
    const [pet, setPet] = useState({})
    const [colors] = useContext(ColorContext)
    const [isCompleted, setIsCompleted] = useState(Boolean)
    const [updateSuccess, setUpdateSuccess] = useState(false)

    const serviceData = props.getServiceQuery.getService ? props.getServiceQuery.getService : {}
    const usersData = props.getUsersQuery.getUsers ? props.getUsersQuery.getUsers : []

    useEffect(() => {
        setPet(serviceData.pet)
    }, [serviceData])

    const renderSideBox = () => {
        if (props.getServiceQuery.loading) {
            return <p>Fetching data...</p>
        } else {
            return <DashboardSideBox user={currentUser} />
        }
    }

    const renderPetProfile = () => {
        if (props.getServiceQuery.loading) {
            return <p>Fetching data...</p>
        } else {
            return <PetProfile petData={serviceData.pet} />
        }
    }

    const handleChange = e => {
        let { id, value } = e.target

        if (id === "sitter") {
            setSitter(value)
        }

        // if (id === "isCompleted") {
        //     setIsCompleted(!value)
        // }
    }

    const handleSubmit = e => {
        e.preventDefault()

        props.updateServiceMutaion({
            variables: { ...serviceData, sitter_id: sitter, isCompleted: isCompleted }
        }).then(res => {
            // console.log(res)
            Swal.fire({
                icon: 'success',
                title: 'Service updated.'
            })
        }).then(
            () => {
                setUpdateSuccess(true)
            }
        ).catch(error => {
            // console.log(error.message)
            Swal.fire({
                icon: "error",
                title: String(error.message)
            });
        })
    }

    const renderSitterOptions = () => {
        if (!props.getUsersQuery.loading) {
            return usersData.map(user => {
                return <option value={user.id}>{user.first_name} {user.last_name}</option>
            })
        }
    }
    console.log(isCompleted)

    if (updateSuccess) {
        return <Redirect to='/dashboard' />
    }

    return (
        <UpdateServiceSection>
            <div className="row md-0 p-0">
                <div className="col-12 col-md-4 m-0 p-0">
                    {renderSideBox()}
                </div>
                <div className="col-12 col-md-8 m-0 p-0 align-self-center text-center">
                    <PetProfileDiv>
                        {renderPetProfile()}
                        {/* <h5 className="mt-5">Asign Sitter</h5> */}

                        <form onSubmit={handleSubmit} className="mt-5">
                            <div className="form-group">
                                <input
                                    type="checkbox"
                                    id="isCompleted"
                                    className="mx-2"
                                    onChange={() => setIsCompleted(!isCompleted)}
                                    checked={isCompleted}
                                />
                                <label htmlFor="isCompleted">Completed</label>
                            </div>
                            <div className="form-group">
                                <select name="sitter" id="sitter" className="form-control w-50 mx-auto" onChange={handleChange} >
                                    <option selected disabled>Select Sitter</option>
                                    {renderSitterOptions()}
                                </select>
                            </div>
                            <ButtonCta title="Assign Sitter" bgColor={colors.primary} color={"white"} colorHover={colors.primaryLight} />
                        </form>
                    </PetProfileDiv>
                </div>
            </div>
        </UpdateServiceSection >
    )
}

export default compose(
    graphql(updateServiceMutaion, { name: "updateServiceMutaion" }),
    graphql(getUsersQuery, { name: "getUsersQuery" }),
    graphql(getServiceQuery, {
        options: (props) => {
            return { variables: { id: props.match.params.id } }
        },
        name: 'getServiceQuery'
    })
)(UpdateService)
