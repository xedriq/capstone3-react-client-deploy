import React, { useContext, useState, useEffect } from 'react'
import { Link, Redirect } from 'react-router-dom'
import styled from 'styled-components'
import { graphql } from 'react-apollo'

//infinite calendar
// import InfiniteCalendar from 'react-infinite-calendar';
// import 'react-infinite-calendar/styles.css'; // only needs to be imported once

//react-dates
// import 'react-dates/initialize';
// import 'react-dates/lib/css/_datepicker.css';
// import { DateRangePicker, SingleDatePicker, DayPickerRangeController } from 'react-dates';

import { ColorContext } from '../ColorContext'

import Swal from "sweetalert2";


// components
import SignupProgress from '../components/SignupProgress'
import SignUpSideBanner from '../components/SignUpSideBanner'
import ButtonCta from '../components/ButtonCta'

// mutation - queries
import { updatePetMutation } from '../queries/mutations'


// loadash
import { flowRight as compose } from 'lodash'

//flatpickr
// import flatpickr from "flatpickr";
import Flatpickr from 'react-flatpickr'


const SaveExitContainer = styled.div`
    position: relative;
    
    a:link,
    a:visited,
    a:hover,
    a:active {
        text-decoration: none;
        color: white;
    }

    .save-exit {
        position: absolute;
        bottom: 40px;
        right: 50%;
        padding: 0;
        transform: translate(50%, -50%);
    }
`

const PetType = styled.div`
    background: ${props => props.color};
`

const SignupPetBasics = (props) => {

    const [colors] = useContext(ColorContext)
    const [petName, setPetName] = useState()
    const [petPhoto, setPetPhoto] = useState('')
    const [breed, setBreed] = useState('')
    const [petBirthday, setPetBirthday] = useState('')
    const [spayedNeutered, setSpayedNeutered] = useState(false)
    const [gender, setGender] = useState('male')
    const [weight, setWeight] = useState(0)
    const [updatedPet, setUpdatedPet] = useState({})
    const [gotoPetBasicSignup, setGotoPetBasicSignup] = useState(false)
    const [petId, setPetId] = useState(localStorage.getItem('pet_id'))
    const [petType, setPetType] = useState(localStorage.getItem('pet_type'))
    const [loading, setLoading] = useState(false)

    const handlePhotoUpload = async (e) => {
        const files = e.target.files
        const data = new FormData()
        data.append('file', files[0])
        data.append('upload_preset', 'pawtastic')
        setLoading(true)
        const res = await fetch(
            'https://api.cloudinary.com/v1_1/dicm79iyu/image/upload',
            {
                method: 'POST',
                body: data
            }
        )
        const file = await res.json()
        setPetPhoto(file.secure_url)
        setLoading(false)
    }
    console.log(petBirthday)
    const handleChange = (e) => {
        let { name, id, value } = e.target

        if (id === "petName") {
            setPetName(value)
        }

        // pet photo handler
        // if (id === "petPhoto") {


        //     setPetPhoto(e.target.files[0])

        // }

        if (id === "breed") {
            setBreed(value)
        }

        if (id === "petBirthday") {
            setPetBirthday(value)
        }

        if (id === "breed") {
            setBreed(value)
        }

        if (name === "gender") {
            setGender(value)
        }

        if (id === "weight") {
            setWeight(value)
        }
    }



    useEffect(() => {
        setUpdatedPet({
            ...updatedPet,
            id: petId,
            pet_type: petType,
            name: petName,
            birthday: petBirthday,
            breed: breed,
            photo: petPhoto,
            gender: gender,
            spayed_neutered: spayedNeutered,
            weight: Number(weight),
            owner_id: localStorage.getItem('user_id')
        })
    }, [petName, breed, petPhoto, gender, spayedNeutered, weight, petBirthday, petId])

    const handleSubmit = (e) => {
        e.preventDefault()

        props.updatePetMutation({
            variables: updatedPet
        }).then((res) => {
            console.log('the PET ' + res)
            Swal.fire({
                icon: 'success',
                title: 'Pet created!'
            })
            localStorage.setItem('pet', JSON.stringify(updatedPet))
        }).then(() => {
            setGotoPetBasicSignup(true)
        }).catch(error => {
            Swal.fire({
                icon: "error",
                title: String(error.message)
            });
        })
    }

    const handleDateChange = (selectedDates) => {
        setPetBirthday(selectedDates)
    }

    if (gotoPetBasicSignup) {
        return <Redirect to='/signup/confirm' />
    }

    return (
        <React.Fragment>
            <PetType color={colors.tertiaryLight} >
                <div className="row">
                    <SaveExitContainer className="col-12 col-md-4 p-0">
                        <SignUpSideBanner
                            photo="/images/signup_image_3.png"
                            content={SignupProgress("pet type")}
                        />

                        <Link to='/' >
                            <p className="save-exit">Save and Exit</p>
                        </Link>

                    </SaveExitContainer>
                    <div className="col-12 col-md-8 p-0 text-center">
                        <h2 className="w-75 mx-auto  my-5">Yay, we love dogs! Give us
                            the basics about your pup.</h2>

                        <form encType="multipart/form-data" className="w-75 mx-auto" onSubmit={handleSubmit}>
                            <div className="row">
                                <div className="col-12 col-md-6">
                                    <div className="form-group">
                                        <label htmlFor="petName">Pet Name</label>
                                        <input
                                            type="text"
                                            className="form-control"
                                            id="petName"
                                            onChange={handleChange}
                                            value={petName}
                                        />
                                    </div>

                                </div>
                                <div className="col-12 col-md-6">
                                    <div className="form-group">
                                        <label htmlFor="petPhoto">Upload photo</label>
                                        <input
                                            type="file"
                                            id="petPhoto"
                                            onChange={handlePhotoUpload}
                                        // value={petPhoto}
                                        />
                                    </div>
                                    <img src={petPhoto} alt="pet photo" style={{ width: '200px' }} />
                                </div>
                            </div>

                            <div className="row">
                                <div className="col-12 col-md-6">
                                    <div className="form-group">
                                        <label htmlFor="breed">Breed</label>
                                        <input
                                            type="text"
                                            className="form-control"
                                            id="breed"
                                            onChange={handleChange}
                                            value={breed}
                                        />
                                    </div>

                                </div>
                                <div className="col-12 col-md-6">
                                    <div className="form-group">
                                        <label htmlFor="petBirthday" className="d-block">Birthday</label>

                                        <input
                                            type="date"
                                            className="form-control"
                                            onChange={handleChange}
                                            id="petBirthday"
                                            value={petBirthday}
                                        />

                                        {/* <input
                                            type="date"
                                            id="petBirthday"

                                            onChange={handleChange}
                                            value={petBirthday}

                                        /> */}

                                    </div>
                                </div>
                            </div>
                            <div className="row">
                                <div className="col-12 col-md-6">
                                    <p>Gender</p>
                                    <div className="form-group">
                                        <input type="radio" id="male" value="male" name="gender" onChange={handleChange} checked={gender === "male" ? true : false} />
                                        <label htmlFor="male">Male</label>
                                    </div>
                                    <div className="form-group">
                                        <input type="radio" id="female" value="female" name="gender" onChange={handleChange} checked={gender === "female" ? true : false} />
                                        <label htmlFor="female">Female</label>
                                    </div>

                                </div>
                                <div className="col-12 col-md-6">
                                    <div className="form-group">
                                        <input type="checkbox" id="spayed_neutered" onChange={() => setSpayedNeutered(!spayedNeutered)} value={spayedNeutered} checked={spayedNeutered} />
                                        <label htmlFor="spayed_neutered">Spayed or Neutered</label>
                                    </div>
                                </div>
                            </div>
                            <div className="row">
                                <div className="col-12">
                                    <div className="form-group">
                                        <label htmlFor="weight">Weight</label>
                                        <input
                                            type="number"
                                            id="weight"
                                            className="form-control"
                                            onChange={handleChange}
                                            value={weight}
                                        />
                                    </div>
                                </div>
                            </div>

                            <div className="row mt-5">
                                <div className="col-12 col-8">
                                    <div className="signup-footer d-md-flex justify-content-around align-items-center w-75 mx-auto">
                                        <Link to="/signup/pet-type">
                                            <ButtonCta
                                                title="Back"
                                                color={colors.greyLight}
                                                bgColor={colors.primary}
                                                colorHover={colors.primaryLight}
                                            />
                                        </Link>

                                        <ButtonCta
                                            title="Next"
                                            color={colors.primary}
                                            bgColor={colors.grey}
                                            colorHover={colors.greyLight}
                                        />
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </PetType>
        </React.Fragment>
    )
}

export default compose(
    graphql(updatePetMutation, { name: "updatePetMutation" })
)(SignupPetBasics)