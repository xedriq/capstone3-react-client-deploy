import React, { useState, useContext, useEffect } from 'react'
import { Link } from 'react-router-dom'
import { graphql } from 'react-apollo'
import { Redirect } from 'react-router-dom'

// components
import SignupProgress from '../components/SignupProgress'
import SignUpSideBanner from '../components/SignUpSideBanner'
import ButtonCta from '../components/ButtonCta'

//context
import { ColorContext } from '../ColorContext'

import Swal from "sweetalert2";

//query
import { getUserQuery } from '../queries/queries'

//mutation
import { updateUserMutation, deleteUserMutation } from '../queries/mutations'

// loadash
import { flowRight as compose } from 'lodash'

const UpdateUser = (props) => {
    const [first_name, setFirstName] = useState('')
    const [last_name, setLastName] = useState('')
    const [phone, setPhone] = useState('')
    const [alt_phone, setAltPhone] = useState('')
    const [street_address, setStreetAddress] = useState('')
    const [city, setCity] = useState('')
    const [zip_code, setZipCode] = useState('')
    const [colors] = useContext(ColorContext)
    const [gotoPetTypeSignup, setGotoPetTypeSignup] = useState(false)
    const [userId, setUserId] = useState('')
    const [password, setPassword] = useState('')
    const [currentUser, setCurrentUser] = useState({})
    const [deleteSuccess, setDeleteSucces] = useState(false)

    let userData = props.getUserQuery.getUser ? props.getUserQuery.getUser : {}

    if (!props.getUserQuery.loading) {
        const setDefault = () => {
            setFirstName(userData.first_name)
            setLastName(userData.last_name)
            setPhone(userData.phone)
            setAltPhone(userData.alt_phone)
            setStreetAddress(userData.street_address)
            setCity(userData.city)
            setZipCode(userData.zip_code)
            setUserId(userData.id)
            setPassword(userData.password)
        }
        if (!first_name) {
            setDefault()
        }
    }

    // useEffect(() => {
    //     setCurrentUser(userData)
    // }, [userData])

    const changeHandler = (e) => {
        let { id, value } = e.target

        if (id === "first_name") {
            setFirstName(value)
        }

        if (id === "last_name") {
            setLastName(value)
        }

        if (id === "phone") {
            setPhone(value)
        }

        if (id === "alt_phone") {
            setAltPhone(value)
        }

        if (id === "street_address") {
            setStreetAddress(value)
        }

        if (id === "city") {
            setCity(value)
        }

        if (id === "zip_code") {
            setZipCode(value)
        }

        // setUser({ ...user, first_name, last_name, phone, alt_phone, street_address, city, zip_code })
    }

    const handleSubmit = (e) => {
        e.preventDefault()

        // let updatedUser = {
        //     id: userData.id,
        //     first_name,
        //     last_name,
        //     phone,
        //     alt_phone,
        //     street_address,
        //     city,
        //     // zip_code
        // }

        // console.log(updatedUser)
        // console.log(userData.id)
        props.updateUserMutation({
            variables: { ...currentUser }
        }).then(res => {
            console.log(res)
            // localStorage.setItem('user', JSON.stringify({ updatedUser, id: userId }))
            // localStorage.setItem('user', JSON.stringify(updatedUser))
            Swal.fire({
                icon: 'success',
                title: 'User account updated!'
            })
        }).then(() => {
            setGotoPetTypeSignup(true)
        }).catch(error => {
            Swal.fire({
                icon: "error",
                title: String(error.message)
            });
        })
    }


    // if (!props.getUserQuery.loading) {
    //     const setDefault = () => {
    //         setFirstName(userData.first_name)
    //         setLastName(userData.last_name)
    //         setPhone(userData.phone)
    //         setAltPhone(userData.alt_phone)
    //         setStreetAddress(userData.street_address)
    //     }

    //     if (!user) {
    //         setDefault()
    //     }
    // }
    const handleDelete = (e) => {
        e.preventDefault()

        Swal.fire({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, delete it!'
        }).then((result) => {
            if (result.value) {
                props.deleteUserMutation({
                    variables: { id: currentUser.id }
                }).then(res => {
                    console.log(res)
                    if (true) {
                        localStorage.clear()
                        return <Redirect to='/' />
                    }
                }).then(() => {
                    setDeleteSucces(true)
                }).catch(error => {
                    Swal.fire({
                        icon: "error",
                        title: String(error.message)
                    });
                })
                Swal.fire(
                    'Deleted!',
                    'Your file has been deleted.',
                    'success'
                )
            }
        })
    }

    if (deleteSuccess) {
        return <Redirect to="/" />
    }

    return (
        <React.Fragment>
            <div className="row">
                <div className="col-12 col-md-4">
                    <SignUpSideBanner
                        photo="/images/signup_image_2.png"
                        content={SignupProgress("user")}
                    />
                </div>
                <div className="col-12 col-md-8">
                    <h2 className="text-center w-75 mx-auto my-5">Hi {currentUser.first_name}! Let's update your details.</h2>
                    <form className="w-75 mx-auto" onSubmit={handleSubmit}>
                        <div className="row">
                            <div className="col-12 col-md-6">
                                <div className="form-group">
                                    <label htmlFor="first_name">First Name</label>
                                    <input
                                        type="text"
                                        className="form-control"
                                        id="first_name"
                                        onChange={changeHandler}
                                        value={first_name} />
                                </div>
                            </div>
                            <div className="col-12 col-md-6">
                                <div className="form-group">
                                    <label htmlFor="last_name">Last Name</label>
                                    <input
                                        type="text"
                                        className="form-control"
                                        id="last_name"
                                        onChange={changeHandler}
                                        value={last_name}
                                    />
                                </div>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col-12 col-md-6">
                                <div className="form-group">
                                    <label htmlFor="phone">Phone</label>
                                    <input
                                        type="number"
                                        className="form-control"
                                        id="phone"
                                        onChange={changeHandler}
                                        value={phone}
                                    />
                                </div>
                            </div>
                            <div className="col-12 col-md-6">
                                <div className="form-group">
                                    <label htmlFor="alt_phone">Alt Phone</label>
                                    <input
                                        type="number"
                                        className="form-control"
                                        id="alt_phone"
                                        onChange={changeHandler}
                                        value={alt_phone}
                                    />
                                </div>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col-12">
                                <div className="form-group">
                                    <label htmlFor="street_address">Street Address</label>
                                    <input
                                        type="text"
                                        className="form-control"
                                        id="street_address"
                                        onChange={changeHandler}
                                        value={street_address}
                                    />
                                </div>
                            </div>

                        </div>
                        <div className="row">
                            <div className="col-12 col-md-6">
                                <div className="form-group">
                                    <label htmlFor="city">City</label>
                                    <input
                                        type="text"
                                        className="form-control"
                                        id="city"
                                        onChange={changeHandler}
                                        value={city}
                                    />
                                </div>
                            </div>
                            <div className="col-12 col-md-6">
                                <div className="form-group">
                                    <label htmlFor="zip_code">Zip Code/Postal Code</label>
                                    <input
                                        type="number"
                                        className="form-control"
                                        id="zip_code"
                                        onChange={changeHandler}
                                        value={zip_code}
                                    />
                                </div>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col-12">
                                <div className="form-group">
                                    <label htmlFor="password">Password</label>
                                    <input
                                        type="password"
                                        className="form-control"
                                        onChange={changeHandler}
                                        value={password}
                                    />
                                </div>
                            </div>
                        </div>
                        <div className="row mt-5">
                            <div className="col-12 col-8">
                                <div className="signup-footer d-md-flex justify-content-between align-items-center">
                                    <Link to="/dashboard">
                                        <ButtonCta
                                            title="Back"
                                            color={colors.greyLight}
                                            bgColor={colors.primary}
                                            colorHover={colors.primaryLight}
                                        />
                                    </Link>

                                    <ButtonCta
                                        title="Update Details"
                                        color={colors.primary}
                                        bgColor={colors.grey}
                                        colorHover={colors.greyLight}
                                    />
                                </div>
                            </div>
                        </div>
                    </form>
                    <form onSubmit={handleDelete}>
                        <div className="row mt-5 justify-content-center">
                            <ButtonCta title="Delete Account" />
                        </div>
                    </form>
                </div>
            </div>
        </React.Fragment >
    )
}

export default compose(
    graphql(updateUserMutation, { name: 'updateUserMutation' }),
    graphql(deleteUserMutation, { name: 'deleteUserMutation' }),
    graphql(getUserQuery, {
        options: (props) => {
            return { variables: { id: props.match.params.id } }
        },
        name: "getUserQuery"
    })
)(UpdateUser)
