import React, { useState, useContext } from 'react'
import { Link, Redirect } from 'react-router-dom'
import { graphql } from 'react-apollo'
import styled from 'styled-components'

import Swal from "sweetalert2";

// loadash
import { flowRight as compose } from 'lodash'

// components 
import SignUpSideBanner from '../components/SignUpSideBanner'
import ButtonCta from '../components/ButtonCta'

//context
import { ColorContext } from '../ColorContext'
import { UserContext } from '../UserContext'

//mutation
import { createUserMutation } from '../queries/mutations'

//query
import { getUserByEmailQuery, getUserQuery } from '../queries/queries'

const whyOurService = () => (
    < ul >
        <li>We’re animal lovers backed by insurance and experience</li>
        <li>Powered by tech, so you can book and pay from our app</li>
        <li>Updates and pics for every visit keep you in the loop</li>
    </ul >
);

const Signup1 = styled.div`
    height: 100vh;
    overflow: hidden;
    background: #F7ECEB;

    form,
    .signup-footer {
        width: 70%;
    }

    .signup-footer {
        // background: #F0F1F7;
        padding: 2em 4em;
        width: 100%;
        bottom: 0;
    }

    li {
        line-height: 2.2;
        margin-bottom: 1em;
    }

`;

const Signup = (props) => {
    const [email, setEmail] = useState('')
    const [password, setPassword] = useState('')
    const [password2, setPassword2] = useState('')
    const [colors] = useContext(ColorContext)
    const [gotoUserRegister, setGotoUserRegister] = useState(false)
    const [user, setUser] = useContext(UserContext)

    // useEffect(() => {
    //     console.log(email)
    //     console.log(password)
    //     console.log(password2)
    // })

    const changeHandler = (e) => {
        let { name, value } = e.target

        if (e.target.name === "email") {
            setEmail(value);
        }

        if (name === "password") {
            setPassword(value)
        }

        if (name === "password2") {
            setPassword2(value)
        }
    }

    const handleSubmit = (e) => {
        e.preventDefault()

        try {

            if (email === "" || password === "" || password2 === "") {
                throw new Error('Fields cannot be empty.')
            }

            if (password !== password2) {
                throw new Error('Passwords should match!')
            } else {
                let newUser = {
                    email: email,
                    password: password
                }

                // query mutations for new user
                props.createUserMutation({
                    variables: newUser,
                    // refetchQueries: [
                    //     { query: getUserByEmailQuery }
                    // ]
                }).then((res) => {
                    setUser(res.data.createUser)
                    localStorage.setItem('user_id', res.data.createUser.id)
                    // localStorage.setItem('user', JSON.stringify(newUser))
                    Swal.fire({
                        icon: 'success',
                        title: 'User account created!'
                    })
                }).then(() => {
                    setGotoUserRegister(true)
                })
            }
        } catch (error) {
            Swal.fire({
                icon: "error",
                title: String(error.message)
            });
        }

    }

    if (gotoUserRegister) {
        return <Redirect to="/signup/user" />
    }

    return (
        <Signup1>
            <div className="row">
                <div className="col-12 col-md-4 p-0">
                    <SignUpSideBanner
                        photo="/images/signup_image_1.png"
                        title="Why our service?"
                        content={whyOurService()}
                    />
                </div>
                <div className="col-12 col-md-8 p-0 align-self-center">
                    <h2 className="text-center my-5">Great! Let’s create your account.</h2>
                    <form onSubmit={handleSubmit} className="mx-auto">
                        <div className="form-group">
                            <label htmlFor="email">Email Address</label>
                            <input
                                type="email"
                                className="form-control"
                                id="email"
                                placeholder="Your email address"
                                onChange={changeHandler}
                                defaultValue={email}
                                name="email"
                            />
                        </div>
                        <div className="row mt-4">
                            <div className="col-12 col-md-6">
                                <div className="form-group">
                                    <label htmlFor="password">Password</label>
                                    <input
                                        type="password"
                                        className="form-control"
                                        id="password"
                                        placeholder="Your password"
                                        onChange={changeHandler}
                                        defaultValue={password}
                                        name="password"
                                    />
                                </div>
                            </div>
                            <div className="col-12 col-md-6">
                                <div className="form-group">
                                    <label htmlFor="password2">Confirm Password</label>
                                    <input
                                        type="password"
                                        className="form-control"
                                        id="password2"
                                        placeholder="Confirm your password"
                                        onChange={changeHandler}
                                        defaultValue={password2}
                                        name="password2"
                                    />
                                </div>
                            </div>
                        </div>
                        <div className="form-group mt-4">
                            <input type="checkbox" id="privacy" className="mr-2" />
                            <label htmlFor="privacy">
                                <small>I have read the Privacy Policy and agree to the Terms of Service.</small>
                            </label>
                        </div>
                        <div className="col-12 col-8">
                            <div className="signup-footer d-md-flex justify-content-around align-items-center">
                                <Link to="/login">
                                    Already a member? Login
                                    </Link>

                                <ButtonCta
                                    title="Sign up"
                                    color={colors.greyLight}
                                    bgColor={colors.primary}
                                    colorHover={colors.primaryLight}
                                />
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </Signup1>

    )
}

export default compose(
    graphql(createUserMutation, { name: 'createUserMutation' }),
    // graphql(createUserMutation, { name: 'createUserMutation' }),
    // graphql(getUserByEmailQuery, { name: 'getUserByEmailQuery' })

)(Signup)