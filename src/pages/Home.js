import React from 'react'

// components
import Header from '../components/Header'
import About from '../components/About'
import Footer from '../components/Footer'


const Home = () => {
    return (
        <React.Fragment>
            <Header />
            <About />
            <Footer />
        </React.Fragment>
    )
}

export default Home