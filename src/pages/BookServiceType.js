import React, { useState } from 'react'
import styled from 'styled-components'
import { Redirect } from 'react-router-dom'
import { graphql } from 'react-apollo'

//component
import SignUpSideBanner from '../components/SignUpSideBanner'
import FooterBooking from '../components/FooterBooking'

// query
import { getPetsQuery } from '../queries/queries'
// mutatuin
import { createServiceMutation } from '../queries/mutations'

// loadash
import { flowRight as compose } from 'lodash'



const BookServiceTypeDiv = styled.div`
    position: relative;

`

export const FooterDiv = styled.div`
    position:absolute;
    width: 100%;
    display: block;
    bottom: 0;
    padding:3em 0;
    
`

function BookServiceType(props) {

    const [serviceName, setServiceName] = useState('')
    const [next, setNext] = useState(false)
    const [petId, setPetId] = useState(props.match.params.petId)
    const [pet, setPet] = useState('')
    const [currentUser] = useState(JSON.parse(localStorage.getItem('user')))

    const petsData = props.getPetsQuery.getPets ? props.getPetsQuery.getPets : []

    const handleChange = (e) => {
        if (e.target.id === "pet") {
            setPet(e.target.value)
        }
        if (e.target.name === "service-name") {
            setServiceName(e.target.value)
        }
    }

    const handleSubmit = (e) => {
        e.preventDefault()
        if (props.match.params === {}) {

        }
        console.log(props.match.params === {} ? 'may param' : 'walang param')
        localStorage.setItem('serviceName', serviceName)
        localStorage.setItem('petId', props.match.params ? petId : pet)
        setNext(true)
    }
    // console.log(props.match.params.petId)
    // useEffect(() => {
    //     localStorage.setItem('serviceName', serviceName)
    // }, [serviceName])

    const renderPetsOption = () => {
        if (props.getPetsQuery.loading) {
            return <p>Fething pets...</p>
        } else {
            return petsData.map(pet => {
                if (pet.owner_id === currentUser.id) {
                    return <option value={pet.id}>{pet.name === null ? `unnamed ${pet.name}` : pet.name}</option>
                }
            })
        }
    }

    if (next) {
        return <Redirect to='/book/service-details' />
    }

    return (
        <BookServiceTypeDiv>
            <div className="row m-0 p-0">
                <div className="col-12 col-md-4 p-0">
                    <SignUpSideBanner content="why our service?" photo='/images/dog2_01.png' />
                </div>
                <div className="col-12 col-md-8 p-0">
                    <h3 className="text-center py-5">We can’t wait to see your pet! How can we help?</h3>
                    <form onSubmit={handleSubmit} className="text-center">
                    {!petId ? (

                        <div className="form-group">
                            <label htmlFor="pet">Create service for: </label>
                            <select name="pet" id="pet" onChange={handleChange} value={pet} >
                                <option value="" disabled selected>Select a pet</option>
                                {renderPetsOption()}
                            </select>
                        </div>
                    ) : ''}
                        <div className="form-group">
                            <input
                                type="radio"
                                name="service-name"
                                id="dog walk" className="mr-2"
                                value="dog walk"
                                onChange={handleChange}
                                checked={serviceName === "dog walk" ? true : false} />
                            <label htmlFor="dog walk">Dog Walk</label>
                        </div>
                        <div className="form-group">
                            <input
                                type="radio"
                                name="service-name"
                                id="drop-in visit"
                                className="mr-2"
                                value="drop-in visit"
                                onChange={handleChange}
                                checked={serviceName === "drop-in visit" ? true : false} />
                            <label htmlFor="drop-in visit">Drop-in Visit</label>
                        </div>

                        <div className="form-group">
                            <input
                                type="radio"
                                name="service-name"
                                id="house sitting"
                                className="mr-2"
                                value="house sitting"
                                onChange={handleChange}
                                checked={serviceName === "house sitting" ? true : false} />
                            <label htmlFor="house sitting">House Sitting</label>
                        </div>
                        <FooterDiv>
                            <FooterBooking backTo="/dashboard" nextTo="/book/service-details" nextTitle="Next" />
                        </FooterDiv>
                    </form>
                </div>
            </div>
        </BookServiceTypeDiv>
    )
}

export default compose(
    graphql(getPetsQuery, { name: 'getPetsQuery' })
)(BookServiceType)
