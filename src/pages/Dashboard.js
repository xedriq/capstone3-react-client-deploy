import React, { useState, useEffect, useContext } from 'react'
import styled from 'styled-components'
import { graphql } from 'react-apollo'
import { uuid } from 'uuidv4'
import { Link, useParams } from 'react-router-dom'

// loadash
import { flowRight as compose } from 'lodash'

import Swal from "sweetalert2";

//compnents
import DashboardSideBox from '../components/dashboard/DashboardSideBox'
import ServiceListItem from '../components/service-list-item/ServiceListItem'
import ServiceDetail from '../components/service-list-item/ServiceDetail'

//query
import { getServicesQuery, getUserQuery } from '../queries/queries'

// mutations
import { deleteServiceMutation } from '../queries/mutations'

//context
import { UserContext } from '../UserContext'

const DashboarDiv = styled.div`
    max-height: 100vh;

    .list-detail {
        min-height: 100vh;
    }


    .scheduledServices,
    .completedServices {
        height: content;
        max-height: 42vh;
        // border: 1px lightgrey;
        // border-style: solid none none none;
        overflow: auto;
    }

    a:link,
    a:hover,
    a:visited,
    a:active {
        text-decoration: none;
        link-style: none;
    }
`


const Dashboard = (props) => {
    const [currentUser, setCurrentUser] = useState(JSON.parse(localStorage.getItem('user')))
    const [isClicked, setIsClicked] = useState(false)
    const [user, setUser] = useContext(UserContext)

    let servicesData = props.getServicesQuery.getServices ? props.getServicesQuery.getServices : []
    let userData = props.getUserQuery.getUser ? props.getUserQuery.getUser : {}

    const scheduledServiceItem = () => {
        if (props.getServicesQuery.loading || props.getUserQuery.loading) {
            return <p className="text-info text-center">fecthing services...</p>
        } else {
            return servicesData.map(service => {
                return userData.pets.map(pet => {
                    if (pet.id === service.pet_id && !service.isCompleted && currentUser.user_type === "user") {
                        return (
                            <div key={uuid()}>
                                <div className="scheduledServices">
                                    <Link to={`/dashboard/${service.id}`} onClick={handleClick} >
                                        <ServiceListItem
                                            key={uuid()}
                                            service={service}
                                        />
                                    </Link>
                                </div>
                            </div>
                        )
                    }
                })
            })
        }
    }

    const completedServiceItem = () => {
        if (props.getServicesQuery.loading || props.getUserQuery.loading) {
            return <p className="text-info text-center">fecthing services...</p>
        } else {
            return servicesData.map(service => {
                return userData.pets.map(pet => {
                    if (service.isCompleted && pet.id === service.pet_id) {
                        return (<div key={uuid()} >
                            <div className="completedServices">
                                <Link to={`/dashboard/${service.id}`} onClick={handleClick} >
                                    <ServiceListItem key={uuid()} service={service} />
                                </Link>
                            </div>
                        </div>
                        )
                    }
                })
            })
        }
    }

    const getAllScheduledServices = () => {
        if (props.getServicesQuery.loading || props.getUserQuery.loading) {
            return <p>Fetching services...</p>
        } else {
            return servicesData.map(service => {
                return (
                    <Link to={`/dashboard/${service.id}`} key={uuid()} onClick={handleClick} >
                        <ServiceListItem key={uuid()} service={service} />
                    </Link>
                )
            })
        }
    }

    const handleDelete = () => {
        console.log(props)

        props.deleteServiceMutation({
            variables: { id: props.match.params.id },
            refetchQueries: [{
                query: getServicesQuery
            }]
        }).then(res => {
            if (res) {
                Swal.fire({
                    icon: 'success',
                    title: 'Service Deleted!'
                })
            }
        }).catch(error => {
            Swal.fire({
                icon: "error",
                title: String(error.message)
            });
        })

    }

    const renderDetails = () => {
        if (!props.match.params.id) {
            return <h5 className="text-center pt-5 sticky-top">Please select a service from the left pane.</h5>
        } else {
            return servicesData.map(service => {
                if (service.id === props.match.params.id) {
                    return <ServiceDetail service={service} key={uuid()} handleDelete={handleDelete} />
                }
            })

        }
    }

    const handleClick = (e) => {
        setIsClicked(!isClicked)
    }

    const renderSideBox = () => {
        if (props.getUserQuery.loading) {
            return <p>Fetching data...</p>
        } else {
            return <DashboardSideBox user={userData} />
        }
    }

    return (
        < DashboarDiv >
            <div className="row p-0 m-0">
                <div className="col-12 col-md-3 m-0 p-0">
                    {renderSideBox()}
                </div>
                <div className="list-detail col-12 col-md-9 p-0">
                    <div className="row m-0 p-0">
                        <div className="col-6 p-0">
                            <h4 className="my-3 text-center">{currentUser.user_type === "user" ? "My Services" : "All Services"}</h4>
                            {currentUser.user_type === "admin" && getAllScheduledServices()}
                            <p className="ml-5 mb-0 p-0">Scheduled Services</p>

                            {
                                scheduledServiceItem && currentUser.user_type === "user"
                                    ? scheduledServiceItem()
                                    : <p className="text-center">No scheduled service yet</p>
                            }
                            <p className="ml-5 mb-0 p-0">Completed Services</p>

                            {
                                completedServiceItem && currentUser.user_type === "user" ?
                                    completedServiceItem()
                                    : <p className="text-center">No completed service yet</p>

                            }
                        </div>
                        <div className="col-6 bg-light m-0 p-0">
                            {renderDetails()}
                        </div>
                    </div>
                </div>
            </div>
        </DashboarDiv >
    )
}

export default compose(
    graphql(getServicesQuery, { name: 'getServicesQuery' }),
    graphql(deleteServiceMutation, { name: 'deleteServiceMutation' }),
    graphql(getUserQuery, {
        options: () => {
            let user = JSON.parse(localStorage.getItem('user'))
            return { variables: { id: user.id } }
        },
        name: 'getUserQuery'
    }),

)(Dashboard)