import React, { useState, useEffect } from 'react'
import styled from 'styled-components'
import { Redirect } from 'react-router-dom'

// components
import SignUpSideBanner from '../components/SignUpSideBanner'
import FooterBooking from '../components/FooterBooking'
import { FooterDiv } from './BookServiceType'

const BookDetailsDiv = styled.div`

`
let today = new Date()

function BookDetails() {
    const [recurring, setRecurring] = useState(false)
    const [startDate, setStartDate] = useState('')
    const [days, setDays] = useState([])
    const [times, setTimes] = useState([])
    const [next, setNext] = useState(false)
    const [notes, setNotes] = useState('')
    const [serviceName] = useState(localStorage.getItem('serviceName'))
    const [service, setService] = useState({})
    const [petId] = useState(localStorage.getItem('petId'))

    useEffect(() => {
        setService({ ...service, petId, recurring, serviceName, startDate, days, times, notes })
    }, [petId, recurring, serviceName, startDate, days, times, notes])


    const handleChange = e => {
        let { id, value } = e.target

        if (id === "start-date") {
            setStartDate(value)
        }

        if (id === "monday" ||
            id === "tuesday" ||
            id === "wednesday" ||
            id === "thursday" ||
            id === "friday" ||
            id === "saturday" ||
            id === "sunday"
        ) {
            setDays([...days, value])
            let index = days.indexOf(value)
            console.log(index)
            if (index !== -1) {
                days.splice(index, 1);
                setDays(days)
            }
        }

        if (id === "morning" ||
            id === "afternoon" ||
            id === "evening"
        ) {
            setTimes([...times, value])
            let index = times.indexOf(value)
            console.log(index)
            if (index !== -1) {
                times.splice(index, 1);
                setTimes(times)
            }
        }

        if (id === 'notes') {
            setNotes(value)
        }

    }

    const handleSubmit = (e) => {
        e.preventDefault()
        localStorage.setItem('service', JSON.stringify(service))
        setNext(true)
    }

    if (next) {
        return <Redirect to='/book/service-confirm' />
    }

    return (
        <BookDetailsDiv>
            <div className="row m-0 p-0">
                <div className="col-12 col-md-4 p-0">
                    <SignUpSideBanner content="why our service?" photo='/images/cat_01.png' />
                </div>
                <div className="col-12 col-md-8 p-0">
                    <h3 className="text-center my-5">Okay, we’ll take Ginger for a walk. Just tell us when!</h3>
                    <form onSubmit={handleSubmit}>
                        <div className="row">
                            <div className="col-12 col-md-6 px-5">
                                <div className="form-group">
                                    <input
                                        type="checkbox"
                                        id="recurring"
                                        value={recurring}
                                        className="mr-2"
                                        onChange={() => { setRecurring(!recurring) }}
                                    />
                                    <label htmlFor="recurring">Recurring?</label>
                                </div>
                            </div>
                            <div className="col-12 col-md-6">
                                <div className="form-group mx-auto">
                                    <label htmlFor="recurring">Start Date</label>
                                    <input
                                        type="date"
                                        id="start-date"
                                        className="ml-2"
                                        onChange={handleChange}
                                        value={startDate} />
                                </div>
                            </div>
                        </div>
                        {/*
                        <div className="row p-0">
                            <div className="col-12 m-0 px-5">
                                <label htmlFor="days">Days</label>
                                <div className="col-12 p-0 d-flex justify-content-around">
                                    <div className="form-group">
                                        <input
                                            type="checkbox"
                                            id="monday"
                                            onChange={handleChange}
                                            value="monday"
                                        />
                                        <label htmlFor="monday">Monday</label>
                                    </div>
                                    <div className="form-group">
                                        <input
                                            type="checkbox"
                                            id="tuesday"
                                            onChange={handleChange}
                                            value="tuesday" />
                                        <label htmlFor="tuesday">Tuesday</label>
                                    </div>
                                    <div className="form-group">
                                        <input
                                            type="checkbox"
                                            id="wednesday"
                                            onChange={handleChange}
                                            value="wednesday" />
                                        <label htmlFor="wednesday">Wednesday</label>
                                    </div>
                                    <div className="form-group">
                                        <input
                                            type="checkbox"
                                            id="thursday"
                                            onChange={handleChange}
                                            value="thursday" />
                                        <label htmlFor="thursday">Thursday</label>
                                    </div>
                                    <div className="form-group">
                                        <input
                                            type="checkbox"
                                            id="friday"
                                            onChange={handleChange}
                                            value="friday" />
                                        <label htmlFor="friday">Friday</label>
                                    </div>
                                    <div className="form-group">
                                        <input
                                            type="checkbox"
                                            id="saturday"
                                            onChange={handleChange}
                                            value="saturday" />
                                        <label htmlFor="saturday">Saturday</label>
                                    </div>
                                    <div className="form-group">
                                        <input
                                            type="checkbox"
                                            id="sunday"
                                            onChange={handleChange}
                                            value="sunday" />
                                        <label htmlFor="sunday">Sunday</label>
                                    </div>
                                </div>
                            </div>
                        </div>

                        */}
                        <div className="row p-0">
                            <div className="col-12 m-0 px-5">
                                <label htmlFor="timesOfDay">Times</label>
                                <div className="col-12 d-flex justify-content-around">
                                    <div className="form-group">
                                        <input
                                            type="checkbox"
                                            id="morning"
                                            onChange={handleChange}
                                            value="morning" />
                                        <label htmlFor="morning">Morning</label>
                                    </div>
                                    <div className="form-group">
                                        <input
                                            type="checkbox"
                                            id="afternoon"
                                            onChange={handleChange}
                                            value="afternoon" />
                                        <label htmlFor="afternoon">Afternoon</label>
                                    </div>
                                    <div className="form-group">
                                        <input
                                            type="checkbox"
                                            id="evening"
                                            onChange={handleChange}
                                            value="evening" />
                                        <label htmlFor="evening">Evening</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                        <div className="row m-0 p-0">
                            <div className="col-12 m-0 px-5">
                                <div className="form-group d-flex flex-column">
                                    <label htmlFor="notes">Notes for your sitter</label>
                                    <textarea
                                        name="notes"
                                        id="notes"
                                        cols="30" rows="4"
                                        placeholder="notes for your sitter"
                                        onChange={handleChange}
                                        value={notes}
                                    >{notes}</textarea>
                                </div>
                            </div>
                        </div>
                        

                        <FooterDiv>
                            <FooterBooking backTo="/book/service-type" nextTo="/book/service-confirm" nextTitle="Next" />
                        </FooterDiv>
                    </form>
                </div>
            </div>
        </BookDetailsDiv>
    )
}

export default BookDetails
