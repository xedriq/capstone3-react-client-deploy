import React, { useContext, useState, useEffect } from 'react'
import { Link, Redirect } from 'react-router-dom'
import styled from 'styled-components'

import { graphql } from 'react-apollo'

// loadash
import { flowRight as compose } from 'lodash'

import Swal from "sweetalert2";

// components
import SignupProgress from '../components/SignupProgress'
import SignUpSideBanner from '../components/SignUpSideBanner'
import ButtonCta from '../components/ButtonCta'

//context
import { ColorContext } from '../ColorContext'
import { UserContext } from '../UserContext'

//mutation
import { createUserMutation, updateUserMutation } from '../queries/mutations'


const SignupUser = styled.div`
    overflow: hidden;
`

const User = (props) => {
    const [first_name, setFirstName] = useState('')
    const [last_name, setLastName] = useState('')
    const [phone, setPhone] = useState('')
    const [alt_phone, setAltPhone] = useState('')
    const [street_address, setStreetAddress] = useState('')
    const [city, setCity] = useState('')
    const [zip_code, setZipCode] = useState('')
    const [colors] = useContext(ColorContext)
    const [user, setUser] = useContext(UserContext)
    const [gotoPetTypeSignup, setGotoPetTypeSignup] = useState(false)

    let userId = localStorage.getItem('user_id')

    useEffect(() => {
        setUser({ ...user, first_name, last_name, phone, alt_phone, street_address, city, zip_code })
    }, [first_name, last_name, phone, alt_phone, street_address, city, zip_code])

    const changeHandler = (e) => {
        let { id, value } = e.target

        if (id === "first_name") {
            setFirstName(value)
        }

        if (id === "last_name") {
            setLastName(value)
        }

        if (id === "phone") {
            setPhone(value)
        }

        if (id === "alt_phone") {
            setAltPhone(value)
        }

        if (id === "street_address") {
            setStreetAddress(value)
        }

        if (id === "city") {
            setCity(value)
        }

        if (id === "zip_code") {
            setZipCode(value)
        }
    }

    const handleSubmit = (e) => {
        e.preventDefault()

        let updatedUser = user

        props.updateUserMutation({
            variables: { ...updatedUser, id: userId }
        }).then(res => {
            // localStorage.setItem('user', JSON.stringify({ updatedUser, id: userId }))
            localStorage.setItem('user', JSON.stringify(updatedUser))
            Swal.fire({
                icon: 'success',
                title: 'User account updated!'
            })
        }).then(() => {

            setGotoPetTypeSignup(true)
        }).catch(error => {
            Swal.fire({
                icon: "error",
                title: String(error.message)
            });
        })
    }

    if (gotoPetTypeSignup) {
        return <Redirect to='/signup/pet-type' />
    }

    return (
        <React.Fragment>
            <SignupUser>
                <div className="row">
                    <div className="col-12 col-md-4">
                        <SignUpSideBanner
                            photo="/images/signup_image_2.png"
                            content={SignupProgress("user")}
                        />
                    </div>
                    <div className="col-12 col-md-8">
                        <h2 className="text-center w-75 mx-auto my-5">Hello! Please tell us a little bit about yourself.</h2>
                        <form className="w-75 mx-auto" onSubmit={handleSubmit}>
                            <div className="row">
                                <div className="col-12 col-md-6">
                                    <div className="form-group">
                                        <label htmlFor="first_name">First Name</label>
                                        <input
                                            type="text"
                                            className="form-control"
                                            id="first_name"
                                            onChange={changeHandler}
                                            value={first_name} />
                                    </div>
                                </div>
                                <div className="col-12 col-md-6">
                                    <div className="form-group">
                                        <label htmlFor="last_name">Last Name</label>
                                        <input
                                            type="text"
                                            className="form-control"
                                            id="last_name"
                                            onChange={changeHandler}
                                            value={last_name}
                                        />
                                    </div>
                                </div>
                            </div>
                            <div className="row">
                                <div className="col-12 col-md-6">
                                    <div className="form-group">
                                        <label htmlFor="phone">Phone</label>
                                        <input
                                            type="number"
                                            className="form-control"
                                            id="phone"
                                            onChange={changeHandler}
                                            value={phone}
                                        />
                                    </div>
                                </div>
                                <div className="col-12 col-md-6">
                                    <div className="form-group">
                                        <label htmlFor="alt_phone">Alt Phone</label>
                                        <input
                                            type="number"
                                            className="form-control"
                                            id="alt_phone"
                                            onChange={changeHandler}
                                            value={alt_phone}
                                        />
                                    </div>
                                </div>
                            </div>
                            <div className="row">
                                <div className="col-12">
                                    <div className="form-group">
                                        <label htmlFor="street_address">Street Address</label>
                                        <input
                                            type="text"
                                            className="form-control"
                                            id="street_address"
                                            onChange={changeHandler}
                                            value={street_address}
                                        />
                                    </div>
                                </div>

                            </div>
                            <div className="row">
                                <div className="col-12 col-md-6">
                                    <div className="form-group">
                                        <label htmlFor="city">City</label>
                                        <input
                                            type="text"
                                            className="form-control"
                                            id="city"
                                            onChange={changeHandler}
                                            value={city}
                                        />
                                    </div>
                                </div>
                                <div className="col-12 col-md-6">
                                    <div className="form-group">
                                        <label htmlFor="zip_code">Zip Code/Postal Code</label>
                                        <input
                                            type="number"
                                            className="form-control"
                                            id="zip_code"
                                            onChange={changeHandler}
                                            value={zip_code}
                                        />
                                    </div>
                                </div>

                            </div>
                            <div className="row mt-5">
                                <div className="col-12 col-8">
                                    <div className="signup-footer d-md-flex justify-content-around align-items-center w-75 mx-auto">
                                        <Link to="/signup">
                                            <ButtonCta
                                                title="Back"
                                                color={colors.greyLight}
                                                bgColor={colors.primary}
                                                colorHover={colors.primaryLight}
                                            />
                                        </Link>

                                        <ButtonCta
                                            title="Next"
                                            color={colors.primary}
                                            bgColor={colors.grey}
                                            colorHover={colors.greyLight}
                                        />

                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </SignupUser>
        </React.Fragment >
    )
}

export default compose(
    graphql(createUserMutation, { name: "createUserMutation" }),
    graphql(updateUserMutation, { name: "updateUserMutation" })
)(User) 